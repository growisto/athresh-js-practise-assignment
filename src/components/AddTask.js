import React, { Component } from 'react';

export default class AddTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      task: '',
    };
  }
  handleChange = (event) => {
    this.setState({ task: event.target.value });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    this.props.addTask(this.state.task);
    this.setState({ task: '' });
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          type="text"
          placeholder="Name..."
          value={this.state.task}
          onChange={this.handleChange}
        />
        <button class="add" type="submit">
          Add Item
        </button>
      </form>
    );
  }
}
